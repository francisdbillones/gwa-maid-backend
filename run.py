from gwa_maid import app
import os

if __name__ == '__main__':
    if os.environ.get('DEV_ENV'):
        app.run(debug=True)
    else:
        app.run()
